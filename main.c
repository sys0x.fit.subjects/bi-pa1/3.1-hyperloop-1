#ifndef __PROGTEST__
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <assert.h>
#endif /* __PROGTEST__ */
typedef struct moznyHyperloop{
    unsigned int s1;
    unsigned int s2;
    unsigned int s3;
    unsigned int used;
}MOZNYHYPERLOOP;


unsigned long long int hyperloop                           (unsigned long long int length,
                                                             unsigned int        s2,
                                                             unsigned int        s3,
                                                             unsigned int        bulkhead,
                                                             unsigned int      * c2,
                                                             unsigned int      * c3 ) {
    MOZNYHYPERLOOP h1, h2;
    h1.used = 0;
    h2.used = 0;
    int pocetKombinaci = 0, pocetS2 = 0, pocetS3 = 0, diffS3=100000, diffS2=100000, kolikratVejde;
    unsigned long long int x=0;


    while (666) {

        while (666) {

            if (+pocetS2 * s2 + pocetS3 * s3 + (+pocetS2 + pocetS3 + 1) * bulkhead == length) {
                if (h1.used == 0) {

                    h1.s2 = pocetS2;
                    h1.s3 = pocetS3;
                    h1.used = 1;
                } else if (h2.used == 0) {

                    h2.s2 = pocetS2;
                    h2.s3 = pocetS3;
                    h2.used = 1;
                    diffS3 = h1.s3 - h2.s3;
                    diffS2 = h1.s2 - h2.s2;
                }printf("%d,%d\n", pocetS2, pocetS3);
                //printf("%d,%d\n", pocetS2, pocetS3);
                if ((pocetS3 - diffS3) >= 0 && (pocetS2 - diffS2) >= 0 && h1.used == 1 && h2.used == 1&&diffS3!=0) {
                    kolikratVejde = pocetS3 / diffS3+1 ;
                    pocetS3 = pocetS3 - diffS3 * kolikratVejde;
                    pocetS2 = pocetS2 - diffS2 * kolikratVejde;
                    pocetKombinaci = pocetKombinaci + kolikratVejde;
                    printf("Kolikrat se vejde:%d\n", kolikratVejde);

                } else{


                    pocetKombinaci++;

                    *c2 = pocetS2;
                    *c3 = pocetS3;

            }


            }else if (pocetS2 * s2 + pocetS3 * s3 + (+pocetS2 + pocetS3) * bulkhead > length) {
                break;
            }
            if (s3 != 0 && s3 != s2 && pocetS3 == 0 && x == 0) {
                pocetS3 = (length - pocetS2 * s2 - (pocetS2) * bulkhead) / (s3 + bulkhead);
                if (pocetS3<=0){
                    break;
                }
                x = 1;
            } else if (s3 != 0 && s3 != s2) {
                pocetS3++;
            } else break;
        }

        pocetS3 = 0;

        if (pocetS2 * s2 + pocetS3 * s3 + (pocetS2 + pocetS3 + 1) * bulkhead > length) {
            break;
        }else if (s2 != 0) {
            pocetS2++;
            x = 0;
        } else break;
    }

    return pocetKombinaci;
}

#ifndef __PROGTEST__
int main ( int argc, char * argv [] )
{
  unsigned int c1, c2;
assert ( hyperloop ( 300, 10, 3, 0, &c1, &c2 ) == 11 && 10 * c1 + 3 * c2 + 0 * (c1 + c2 + 1) == 300 );
  assert ( hyperloop ( 123456, 8, 3, 3, &c1, &c2 ) == 1871
           && 8 * c1 + 3 * c2 + 3 * (c1 + c2 + 1) == 123456 );
  assert ( hyperloop ( 127, 12, 8, 0, &c1, &c2 ) == 0 );
  assert ( hyperloop ( 127, 12, 4, 3, &c1, &c2 ) == 1
           && 12 * c1 + 4 * c2 + 3 * (c1 + c2 + 1) == 127 );
  assert ( hyperloop ( 100, 35, 0, 10, &c1, &c2 ) == 1
           && c2 == 0
           && 35 * c1 + 10 * (c1 + 1) == 100 );
  assert ( hyperloop ( 110, 30, 30, 5, &c1, &c2 ) == 1
           && 30 * c1 + 30 * c2 + 5 * (c1 + c2 + 1) == 110 );
  c1 = 2;
  c2 = 7;
  assert ( hyperloop ( 110, 30, 30, 0, &c1, &c2 ) == 0 && c1 == 2 && c2 == 7 );
  c1 = 4;
  c2 = 8;
  assert ( hyperloop ( 9, 1, 2, 10, &c1, &c2 ) == 0 && c1 == 4 && c2 == 8 );
    assert ( hyperloop ( 993000, 1, 2, 0, &c1, &c2) == 496501 && 1 * c1 + 2 * c2 + 0 * (c1 + c2 + 1) == 993000 );
    c1 = 4; c2 = 8;
    assert ( hyperloop ( 87791, 141, 81, 24, &c1, &c2 ) == 0 && c1 == 4 && c2 == 8 );
    c1 = 4; c2 = 8;
    assert ( hyperloop (780, 0, 0, 390, &c1, &c2) == 0 && c1 == 4 && c2 == 8 );
    assert ( hyperloop (742, 0, 742, 0, &c1, &c2) == 1 && 0 * c1 + 742 * c2 + 0 * (c1 + c2 + 1) == 742 );
    assert ( hyperloop (285, 285, 285, 0, &c1, &c2) == 1 && 285 * c1 + 285 * c2 + 0 * (c1 + c2 + 1) == 285 );
    assert ( hyperloop (2406, 802, 1604, 0, &c1, &c2) == 2 && 802 * c1 + 1604 * c2 + 0 * (c1 + c2 + 1) == 2406 );
    assert ( hyperloop (81644, 40, 163, 14, &c1, &c2) == 26 && 40 * c1 + 163 * c2 + 14 * (c1 + c2 + 1) == 81644 );
    c1 = 4; c2 = 8;

    assert ( hyperloop (1760, 0, 0, 880, &c1, &c2) == 0 && c1 == 4 && c2 == 8 );
    assert ( hyperloop (263000, 1, 2, 1, &c1, &c2) == 43833 && 1 * c1 + 2 * c2 + 1 * (c1 + c2 + 1) == 263000 );
   // assert(hyperloop ( 993000, 1, 2, 0, &c1, &c2 ) ⇒ r=496501, s=496500);
    assert ( hyperloop (985, 0, 0, 985, &c1, &c2) == 1 && 0 * c1 + 0 * c2 + 985 * (c1 + c2 + 1) == 985 );
  return 0;
}
#endif /* __PROGTEST__ */